import collections
import contextlib
import dataclasses
import datetime
import os
import time
import typing
from collections import defaultdict, Counter
from enum import IntEnum
from typing import (
    List,
    Optional,
    Callable,
    TYPE_CHECKING,
    Mapping,
    Sequence,
    cast,
)

from lsprotocol.types import Position, Range, Diagnostic, DiagnosticSeverity, TextEdit

from debputy.commands.debputy_cmd.output import OutputStylingBase
from debputy.filesystem_scan import VirtualPathBase
from debputy.lsp.diagnostics import LintSeverity
from debputy.lsp.vendoring._deb822_repro import Deb822FileElement, parse_deb822_file
from debputy.packages import SourcePackage, BinaryPackage
from debputy.plugin.api.feature_set import PluginProvidedFeatureSet
from debputy.util import _warn

if TYPE_CHECKING:
    from debputy.lsp.text_util import LintCapablePositionCodec
    from debputy.lsp.style_prefs import (
        StylePreferenceTable,
        EffectivePreference,
    )


LinterImpl = Callable[["LintState"], Optional[List[Diagnostic]]]
FormatterImpl = Callable[["LintState"], Optional[Sequence[TextEdit]]]


class LintState:

    @property
    def plugin_feature_set(self) -> PluginProvidedFeatureSet:
        raise NotImplementedError

    @property
    def doc_uri(self) -> str:
        raise NotImplementedError

    @property
    def source_root(self) -> Optional[VirtualPathBase]:
        raise NotImplementedError

    @property
    def debian_dir(self) -> Optional[VirtualPathBase]:
        raise NotImplementedError

    @property
    def path(self) -> str:
        raise NotImplementedError

    @property
    def content(self) -> str:
        raise NotImplementedError

    @property
    def lines(self) -> List[str]:
        raise NotImplementedError

    @property
    def position_codec(self) -> "LintCapablePositionCodec":
        raise NotImplementedError

    @property
    def parsed_deb822_file_content(self) -> Optional[Deb822FileElement]:
        raise NotImplementedError

    @property
    def source_package(self) -> Optional[SourcePackage]:
        raise NotImplementedError

    @property
    def binary_packages(self) -> Optional[Mapping[str, BinaryPackage]]:
        raise NotImplementedError

    @property
    def style_preference_table(self) -> "StylePreferenceTable":
        raise NotImplementedError

    @property
    def effective_preference(self) -> Optional["EffectivePreference"]:
        raise NotImplementedError


@dataclasses.dataclass(slots=True)
class LintStateImpl(LintState):
    plugin_feature_set: PluginProvidedFeatureSet = dataclasses.field(repr=False)
    style_preference_table: "StylePreferenceTable" = dataclasses.field(repr=False)
    source_root: Optional[VirtualPathBase]
    debian_dir: Optional[VirtualPathBase]
    path: str
    content: str
    lines: List[str]
    source_package: Optional[SourcePackage] = None
    binary_packages: Optional[Mapping[str, BinaryPackage]] = None
    effective_preference: Optional["EffectivePreference"] = None
    _parsed_cache: Optional[Deb822FileElement] = None

    @property
    def doc_uri(self) -> str:
        path = self.path
        abs_path = os.path.join(os.path.curdir, path)
        return f"file://{abs_path}"

    @property
    def position_codec(self) -> "LintCapablePositionCodec":
        return LINTER_POSITION_CODEC

    @property
    def parsed_deb822_file_content(self) -> Optional[Deb822FileElement]:
        cache = self._parsed_cache
        if cache is None:
            cache = parse_deb822_file(
                self.lines,
                accept_files_with_error_tokens=True,
                accept_files_with_duplicated_fields=True,
            )
            self._parsed_cache = cache
        return cache


class LintDiagnosticResultState(IntEnum):
    REPORTED = 1
    FIXABLE = 2
    FIXED = 3


@dataclasses.dataclass(slots=True, frozen=True)
class LintDiagnosticResult:
    diagnostic: Diagnostic
    result_state: LintDiagnosticResultState
    invalid_marker: Optional[RuntimeError]
    is_file_level_diagnostic: bool
    has_broken_range: bool
    missing_severity: bool


class LintReport:

    def __init__(self) -> None:
        self.diagnostics_count: typing.Counter[DiagnosticSeverity] = Counter()
        self.diagnostics_by_file: Mapping[str, List[LintDiagnosticResult]] = (
            defaultdict(list)
        )
        self.number_of_invalid_diagnostics: int = 0
        self.number_of_broken_diagnostics: int = 0
        self.lint_state: Optional[LintState] = None
        self.start_timestamp = datetime.datetime.now()
        self.durations: Mapping[str, float] = collections.defaultdict(lambda: 0.0)
        self._timer = time.perf_counter()

    @contextlib.contextmanager
    def line_state(self, lint_state: LintState) -> typing.Iterable[None]:
        previous = self.lint_state
        if previous is not None:
            path = previous.path
            duration = time.perf_counter() - self._timer
            self.durations[path] += duration

        self.lint_state = lint_state

        try:
            self._timer = time.perf_counter()
            yield
        finally:
            now = time.perf_counter()
            duration = now - self._timer
            self.durations[lint_state.path] += duration
            self._timer = now
            self.lint_state = previous

    def report_diagnostic(
        self,
        diagnostic: Diagnostic,
        *,
        result_state: LintDiagnosticResultState = LintDiagnosticResultState.REPORTED,
        in_file: Optional[str] = None,
    ) -> None:
        lint_state = self.lint_state
        assert lint_state is not None
        if in_file is None:
            in_file = lint_state.path
        severity = diagnostic.severity
        missing_severity = False
        error_marker: Optional[RuntimeError] = None
        if severity is None:
            self.number_of_invalid_diagnostics += 1
            severity = DiagnosticSeverity.Warning
            diagnostic.severity = severity
            missing_severity = True

        lines = lint_state.lines
        diag_range = diagnostic.range
        start_pos = diag_range.start
        end_pos = diag_range.start
        is_file_level_diagnostic = _is_file_level_diagnostic(
            lines,
            start_pos.line,
            start_pos.character,
            end_pos.line,
            end_pos.character,
        )
        has_broken_range = not is_file_level_diagnostic and (
            end_pos.line > len(lines) or start_pos.line < 0
        )

        if has_broken_range or missing_severity:
            error_marker = RuntimeError("Registration Marker for invalid diagnostic")

        diagnostic_result = LintDiagnosticResult(
            diagnostic,
            result_state,
            error_marker,
            is_file_level_diagnostic,
            has_broken_range,
            missing_severity,
        )

        self.diagnostics_by_file[in_file].append(diagnostic_result)
        self.diagnostics_count[severity] += 1
        self.process_diagnostic(in_file, lint_state, diagnostic_result)

    def process_diagnostic(
        self,
        filename: str,
        lint_state: LintState,
        diagnostic_result: LintDiagnosticResult,
    ) -> None:
        # Subclass hook
        pass

    def finish_report(self) -> None:
        # Subclass hook
        pass


_LS2DEBPUTY_SEVERITY: Mapping[DiagnosticSeverity, LintSeverity] = {
    DiagnosticSeverity.Error: "error",
    DiagnosticSeverity.Warning: "warning",
    DiagnosticSeverity.Information: "informational",
    DiagnosticSeverity.Hint: "pedantic",
}


_TERM_SEVERITY2TAG = {
    DiagnosticSeverity.Error: lambda fo, lint_tag=None: fo.colored(
        lint_tag if lint_tag else "error",
        fg="red",
        bg="black",
        style="bold",
    ),
    DiagnosticSeverity.Warning: lambda fo, lint_tag=None: fo.colored(
        lint_tag if lint_tag else "warning",
        fg="yellow",
        bg="black",
        style="bold",
    ),
    DiagnosticSeverity.Information: lambda fo, lint_tag=None: fo.colored(
        lint_tag if lint_tag else "informational",
        fg="blue",
        bg="black",
        style="bold",
    ),
    DiagnosticSeverity.Hint: lambda fo, lint_tag=None: fo.colored(
        lint_tag if lint_tag else "pedantic",
        fg="green",
        bg="black",
        style="bold",
    ),
}


def debputy_severity(diagnostic: Diagnostic) -> LintSeverity:
    lint_tag: Optional[LintSeverity] = None
    if isinstance(diagnostic.data, dict):
        lint_tag = cast("LintSeverity", diagnostic.data.get("lint_severity"))

    if lint_tag is not None:
        return lint_tag
    severity = diagnostic.severity
    if severity is None:
        return "warning"
    return _LS2DEBPUTY_SEVERITY.get(severity, "warning")


class TermLintReport(LintReport):

    def __init__(self, fo: OutputStylingBase) -> None:
        super().__init__()
        self.fo = fo

    def finish_report(self) -> None:
        # Nothing to do for now
        pass

    def process_diagnostic(
        self,
        filename: str,
        lint_state: LintState,
        diagnostic_result: LintDiagnosticResult,
    ) -> None:
        diagnostic = diagnostic_result.diagnostic
        fo = self.fo
        severity = diagnostic.severity
        assert severity is not None
        if diagnostic_result.result_state != LintDiagnosticResultState.FIXED:
            tag_unresolved = _TERM_SEVERITY2TAG[severity]
            lint_tag: Optional[LintSeverity] = debputy_severity(diagnostic)
            tag = tag_unresolved(fo, lint_tag)
        else:
            tag = fo.colored(
                "auto-fixing",
                fg="magenta",
                bg="black",
                style="bold",
            )
        start_line = diagnostic.range.start.line
        start_position = diagnostic.range.start.character
        end_line = diagnostic.range.end.line
        end_position = diagnostic.range.end.character
        has_fixit = ""
        lines = lint_state.lines
        line_no_width = len(str(len(lines)))
        if diagnostic_result.result_state == LintDiagnosticResultState.FIXABLE:
            has_fixit = " [Correctable via --auto-fix]"
        print(
            f"{tag}: File: {filename}:{start_line+1}:{start_position}:{end_line+1}:{end_position}: {diagnostic.message}{has_fixit}",
        )
        if diagnostic_result.missing_severity:
            _warn(
                "  This warning did not have an explicit severity; Used Warning as a fallback!"
            )
        if diagnostic_result.result_state == LintDiagnosticResultState.FIXED:
            # If it is fixed, there is no reason to show additional context.
            return
        if diagnostic_result.is_file_level_diagnostic:
            print("    File-level diagnostic")
            return
        if diagnostic_result.has_broken_range:
            _warn(
                "Bug in the underlying linter: The line numbers of the warning does not fit in the file..."
            )
            return
        lines_to_print = _lines_to_print(diagnostic.range)
        if lines_to_print == 1:
            line = _highlight_range(fo, lines[start_line], start_line, diagnostic.range)
            print(f"    {start_line+1:{line_no_width}}: {line}")
        else:
            for line_no in range(start_line, end_line):
                line = _highlight_range(fo, lines[line_no], line_no, diagnostic.range)
                print(f"    {line_no+1:{line_no_width}}: {line}")


class LinterPositionCodec:

    def client_num_units(self, chars: str):
        return len(chars)

    def position_from_client_units(
        self, lines: List[str], position: Position
    ) -> Position:

        if len(lines) == 0:
            return Position(0, 0)
        if position.line >= len(lines):
            return Position(len(lines) - 1, self.client_num_units(lines[-1]))
        return position

    def position_to_client_units(
        self, _lines: List[str], position: Position
    ) -> Position:
        return position

    def range_from_client_units(self, _lines: List[str], range: Range) -> Range:
        return range

    def range_to_client_units(self, _lines: List[str], range: Range) -> Range:
        return range


LINTER_POSITION_CODEC = LinterPositionCodec()


def _lines_to_print(range_: Range) -> int:
    count = range_.end.line - range_.start.line
    if range_.end.character > 0:
        count += 1
    return count


def _highlight_range(
    fo: OutputStylingBase, line: str, line_no: int, range_: Range
) -> str:
    line_wo_nl = line.rstrip("\r\n")
    start_pos = 0
    prefix = ""
    suffix = ""
    if line_no == range_.start.line:
        start_pos = range_.start.character
        prefix = line_wo_nl[0:start_pos]
    if line_no == range_.end.line:
        end_pos = range_.end.character
        suffix = line_wo_nl[end_pos:]
    else:
        end_pos = len(line_wo_nl)

    marked_part = fo.colored(line_wo_nl[start_pos:end_pos], fg="red", style="bold")

    return prefix + marked_part + suffix


def _is_file_level_diagnostic(
    lines: List[str],
    start_line: int,
    start_position: int,
    end_line: int,
    end_position: int,
) -> bool:
    if start_line != 0 or start_position != 0:
        return False
    line_count = len(lines)
    if end_line + 1 == line_count and end_position == 0:
        return True
    return end_line == line_count and line_count and end_position == len(lines[-1])
